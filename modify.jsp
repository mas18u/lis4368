<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 12-19-20, 17:54:52 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Manol Sanchez">
	<link rel="icon" href="favicon.ico">

	<title>LIS 4368</title>

	<%@ include file="/css/include_css.jsp" %>		

	<style type="text/css">
		h2
		{
			margin: 0;     
			color: #666;
			padding-top: 50px;
			font-size: 52px;
			font-family: "trebuchet ms", sans-serif;    
		}
		.item
		{
			background: #333;    
			text-align: center;
			height: 300px !important;
		}
		.carousel
		{
		  margin: 20px 0px 20px 0px;
		}
		.bs-example
		{
		  margin: 20px;
		
		}
		.navbar-inverse {
			background-image: linear-gradient(to bottom, #FF8045 0, #FF8045 100%);
		}
		.navbar-inverse .navbar-nav>li>a {
			color:azure;
		}
		.navbar-inverse .navbar-brand {
			color:azure;
		}
		body {
			background-color:rgb(255, 255, 245);
		}
		</style>

</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->	
	
	<%@ include file="/global/nav_global.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="/p2/global/header.jsp" %>
					</div>

					<h2>Update Customer</h2>

					<!-- instructional message for users completing form input -->
					<p><i><span style='color: red; background-color: yellow; font-weight: bold; font-variant:small-caps;'>All text boxes required except Notes.</span></i></p>

					<% //for debugging, test input (test servlet provided): action="testInput" %>											
					<form id="edit_customer_form" method="post" class="form-horizontal" action="customerAdmin">
						
						<input type="hidden" name="update_customer" value="${user.id}" />
						
						<div class="form-group">
							<label class="col-sm-4 control-label">FName:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="15" name="fname" value="${user.fname}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">LName:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="30" name="lname" value="${user.lname}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Street:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="30" name="street" value="${user.street}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">City:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="30" name="city" value="${user.city}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Zip:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="9" name="zip" value="${user.zip}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Phone:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="10" name="phone" value="${user.phone}" />
							</div>
						</div>

						
						<div class="form-group">
							<label class="col-sm-4 control-label">Email:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="100" name="email" value="${user.email}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Balance:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="7" name="balance" value="${user.balance}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Total Sales:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="30" name="total_sales" value="${user.totalSales}" />
							</div>
						</div>

						<div class="form-group">
							<label class="col-sm-4 control-label">Notes:</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" maxlength="255" name="notes" value="${user.notes}" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-6 col-sm-offset-3">
								<button type="submit" class="btn btn-primary" name="signup" value="Update">Update</button>
							</div>
						</div>
					</form>

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%--@ include file="/js/include_js.jsp" --%>		

<script type="text/javascript">
$(document).ready(function() {

	$('#edit_customer_form').formValidation({
			message: 'This value is not valid',
			icon: {
					valid: 'fa fa-check',
					invalid: 'fa fa-times',
					validating: 'fa fa-refresh'
			},
			fields: {
		
		fname: {
					validators: {
							notEmpty: {
									message: 'First name required'
							},
							stringLength: {
									min: 1,
									max: 15,
									message: 'First name no more than 15 characters'
							},
							regexp: {
								//http://www.regular-expressions.info/
								//http://www.regular-expressions.info/quickstart.html
								//http://www.regular-expressions.info/shorthand.html
								//http://stackoverflow.com/questions/13283470/regex-for-allowing-alphanumeric-and-space
								//alphanumeric (also, "+" prevents empty strings):
								regexp: /^[a-zA-Z\-]+$/,
								message: 'First name can only contain letters and hyphens.'
							},									
					},
			},

		lname: {
					validators: {
							notEmpty: {
									message: 'Last name required'
							},
							stringLength: {
									min: 1,
									max: 30,
									message: 'Last name no more than 30 characters'
							},
							regexp: {
								regexp: /^[a-zA-Z\-]+$/,
								message: 'Last name can only contain letters and hyphens'
							},									
					},
			},

			street: {
					validators: {
							notEmpty: {
									message: 'Street Required'
							},
							stringLength: {
									min: 1,
									max: 30,
									message: 'Street no more than 30 characters'
							},
							regexp: {
								regexp: /^[a-zA-Z ]+$/,
								message: 'Street can only contain letters'
							},									
					},
			},

			city: {
					validators: {
							notEmpty: {
									message: 'City Required'
							},
							stringLength: {
									min: 1,
									max: 30,
									message: 'City no more than 30 characters'
							},
							regexp: {
								regexp: /^[a-zA-Z ]+$/,
								message: 'City can only contain letters'
							},									
					},
			},

			state: {
					validators: {
							notEmpty: {
									message: 'State Required'
							},
							stringLength: {
									min: 2,
									max: 2,
									message: 'State must contain 2 characters'
							},
							regexp: {
								regexp: /^[a-zA-Z ]+$/,
								message: 'State can only contain letters'
							},									
					},
			},

			
			
			zip: {
					validators: {
							notEmpty: {
									message: 'Zip Required'
							},
							stringLength: {
									min: 5,
									max: 9,
									message: 'Zip must be between 5 and 9 characters'
							},
							regexp: {
								regexp: /^[0-9]+$/,
								message: 'Zip can only contain numbers'
							},									
					},
			},

			phone: {
					validators: {
							notEmpty: {
									message: 'Phone Required'
							},
							stringLength: {
									min: 10,
									max: 10,
									message: 'Phone must be 10 digits, inluding area code'
							},
							regexp: {
								regexp: /^[0-9]+$/,
								message: 'Phone can only contain numbers'
							},									
					},
			},

			balance: {
					validators: {
							notEmpty: {
									message: 'Balance Required'
							},
							stringLength: {
									min: 0,
									max: 7,
									message: 'Balance no more than 6 characters'
							},
							regexp: {
								regexp:/^\d*\.?\d*$/,
								message: 'Balance can only contain numbers and decimal point'
							},									
					},
			},

			sales: {
					validators: {
							notEmpty: {
									message: 'Total Sales Required'
							},
							stringLength: {
									min: 0,
									max: 7,
									message: 'Total Sales no more than 6 characters'
							},
							regexp: {
								regexp:/^\d*\.?\d*$/,
								message: 'Total Sales can only contain numbers and decimal point'
							},									
					},
			},
			
			
			email: {
					validators: {
							notEmpty: {
									message: 'Email address is required'
							},

							/*
							//built-in e-mail validator, comes with formValidation.min.js
							//using regexp instead (below)
							emailAddress: {
									message: 'Must include valid email address'
							},
							*/
						
							stringLength: {
									min: 1,
									max: 100,
									message: 'Email no more than 100 characters'
							},
							regexp: {
							regexp: /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/,
								message: 'Must include valid email'
							},																		
					},
			},
	}
	});
});
</script>

</body>
</html>
