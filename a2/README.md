# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### Assignment #2 Requirements:

*Four Parts*

1. Make and display "Hello" webpage
2. Make and display an index homepage
3. Make a HelloServlet
4. Make a QueryBook servlet that pulls data from external database.  
This website will allow the user to select an author and the page will display the books by that author and how much they cost.

*Links*  
a. http://localhost:9999/hello  
b. http://localhost:9999/hello/HelloHome.html  
c. http://localhost:9999/hello/sayhello  
d. http://localhost:9999/hello/querybook.html  
[http://localhost:9999/lis4368/](http://localhost:9999/lis4368/)

#### Assignment Screenshots:

*Screenshot of running hello index*:
![index] (img/index.html.png)

*Screenshot of running sayHello with RNG on page refresh*:
![sayHello] (img/sayHello.png)

*Screenshot of running queryBook checkbox selection*:
![queryBookSelection] (img/queryBookSelected.png)

*Screenshot of queryResults showing webpage accesing MySQL data:
![queryResults] (img/queryResults.png)

*Screenshot of Index.jsp site with screenshots*
![index] (img/index.png)