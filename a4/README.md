# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### Assignment #4 Requirements:

1. Implement server side data validation for user entered data  
2. Include error page if submitted form is empty  
3. Java Skillset 10 Count Characters  
4. Java Skillset 11 Write/Read Count words  
5. Java Skillset 12 ASCII App  
	
#### Assignment Screenshots:

*Failed Validation*:
![failedValidation] (img/failedValidation.png)

*Passed Validation*:
![passedValidation] (img/passedValidation.png)

*Count Characters Skillset*:
![countCharacters] (img/countCharacters.png)

*Write/Read Count Words Skillset*:
![fileWriteReadCountWords] (img/fileWriteReadCountWords.png)  

*ASCII App Skillset 1*:  
![ascii1] (img/ascii1.png)  

*ASCII App Skillset 2*:  
![ascii2] (img/ascii2.png)  