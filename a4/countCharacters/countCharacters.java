import java.util.Scanner;
import java.lang.*;

public class countCharacters
{

    public static void main(String args[])
    {
        Scanner scnr = new Scanner(System.in);
        String input;
        int letterCount = 0;
        int spaceCount = 0;
        int numberCount = 0;
        int otherCount = 0;

        System.out.println("Program counts number and types of characters: That is, letters, spaces, numbers, and other characters.\n");
        System.out.print("Please enter string: ");
        input = scnr.nextLine();

        for (int i = 0; i < input.length(); i++) {
            if (Character.isLetter(input.charAt(i))){
                letterCount++;
            }
            else if (Character.isDigit(input.charAt(i))) {
                numberCount++;
            }
            else if (Character.isSpaceChar(input.charAt(i))){
                spaceCount++;
            }
            else {
                otherCount++;
            }

        }
        System.out.println("\n\nYour string: " + "\"" + input + "\"" + " has the following number and types of characters:");
        System.out.println("Letter(s): " + letterCount);
        System.out.println("Space(s): " + spaceCount);
        System.out.println("Number(s): " + numberCount);
        System.out.println("Other character(s): " + otherCount);
    }
}