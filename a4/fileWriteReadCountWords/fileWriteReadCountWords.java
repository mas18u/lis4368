import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class fileWriteReadCountWords
{

  static final int OUT = 0;
  static final int IN = 1;

    public static void main(String args[])
    {

    String input;
    int wordCount = 0;
    int i = 0;
    int state = OUT;
    Scanner scnr = new Scanner(System.in);
    String filecountwords = ("C:\\tomcat\\webapps\\lis4368\\a4\\fileWriteReadCountWords\\filecountwords.txt");
    
    System.out.println("Program captures user input, writes to and reads from same file, and counts number of words in file.\n");
    System.out.print("Please enter text: ");
    input = scnr.nextLine(); 

    while (i < input.length()) {

      if(input.charAt(i) == ' ') {
        state = OUT;
      }
      else if(state == OUT) {
        state = IN;
        wordCount++;
      }
      i++;
    }
    
    try{
      PrintWriter outputStream = new PrintWriter(filecountwords);
      outputStream.print(input);
      System.out.println("Saved to file \"filecountwords.txt\"");
      outputStream.close();
      System.out.println("Number of words: " + wordCount);
    }
    catch (FileNotFoundException e) {
      e.printStackTrace();
    }

    
    
   }

}