import java.util.InputMismatchException;
import java.util.Scanner;
import java.io.IOException;

public class ascii {
    public static void main(String[] args) {

        int input = 0;
        boolean done = false;
        Scanner scnr = new Scanner(System.in);

        System.out.println("Printing characters A-Z as ASCII values:");

        for (int i = 65; i <= 90; i++) {
            System.out.println("Character " + (char)i + " has ascii value " + i);
           } 

        System.out.println("\nPrinting ASCII values 48-122 as characters");

        for (int i = 48; i <= 122; i++) {
            System.out.println("ASCII VALUE " + i + " has character value " + (char)i);
        }

       
        System.out.print("\nAllowing user ASCII value input:\nPlease enter ASCII value (32 - 127): ");
        do {

            try {    
               input = scnr.nextInt();
               while((input >= 127 ) || (input <= 32)) {
                    System.out.println("ASCII value must be >= 32 and <= 127.\n");
                    System.out.print("Please enter ASCII value (32 - 127): ");
                    input = scnr.nextInt();
                }
                done = true;
            }
            catch (InputMismatchException e) {
                System.out.println("Invalid integer--ASCII value must be a number.\n");
                System.out.print("Please enter ASCII value (32 - 127): ");
                scnr.next();
            }
        }while(done == false);
        System.out.println("\nASCII value " + input + " has character value " + (char)input + "\n");
    }
}