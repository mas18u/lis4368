<br />
<%@ page import="java.util.GregorianCalendar, java.util.Calendar" %>
<%  
    GregorianCalendar currentDate = new GregorianCalendar();
    int currentYear = currentDate.get(Calendar.YEAR);
%>
&copy; Copyright <%= currentYear %> Manolo Sanchez <br> <a href="https://www.linkedin.com/in/manolo-sanchez/">LinkedIn</a>
