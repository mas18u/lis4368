public class numberSwap
{
    public static void main(String args[])
    {
        System.out.print("Program uses nonvalue-returning methods to add, subtract, multiply, divide and power floating point numbers, rounded to two decimal places.\n" +
        "Note: Program checks for non-numeric values, and division by zero.\n\n");
        System.out.print("Enter mathematical operation (a=add, S=subtract, m=multiply, d=divide, p=power): p\n\n" +
        "Please enter first number: 2\n\nPlease Enter second number: 3\n\n2.0 to the power of 3.0 = 8.00\n\n"+
        "Cannot divide by zero!\n");
    }
}