# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### Assignment #5 Requirements:

1. Implement cross side scripting protection on form
2. Add user entry from website onto SQL database
3. Java Skillset 13 Number Sw  
4. Java Skillset 14 Largest of Three Numbers 
5. Java Skillset 15 Simple Calculator Using Methods
	
#### Assignment Screenshots:

*Valid User Form Entry customerform.jsp*:  
![Form Entry] (img/validEntry.png)

*Passed Validation thanks.jsp*:  
![passed] (img/passed.png)

*SQL Database Entry*:  
![Entry] (img/databaseEntry.png)  

*Number Swap App Java Skillset*:  
![ss13] (img/ss13.png)  

*Largest of Three Numbers Skillset*:  
![ss14] (img/ss14.png)  

*Simple Calculator Using Methods 1*:  
![ss15_1] (img/ss15_1.png)  

*Simple Calculator Using Methods 2*:  
![ss15_2] (img/ss15_2.png)  