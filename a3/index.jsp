<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 19:40:00 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Mark K. Jowett, Ph.D.">
	<link rel="icon" href="favicon.ico">

	<title>LIS4368 - Assignment1</title>

	<%@ include file="/css/include_css.jsp" %>		
	
	<style type="text/css">
		h2
		{
			margin: 0;     
			color: #666;
			padding-top: 50px;
			font-size: 52px;
			font-family: "trebuchet ms", sans-serif;    
		}
		.item
		{
			background: #333;    
			text-align: center;
			height: 300px !important;
		}
		.carousel
		{
		  margin: 20px 0px 20px 0px;
		}
		.bs-example
		{
		  margin: 20px;
		
		}
		.navbar-inverse {
			background-image: linear-gradient(to bottom, #FF8045 0, #FF8045 100%);
		}
		.navbar-inverse .navbar-nav>li>a {
			color:azure;
		}
		.navbar-inverse .navbar-brand {
			color:azure;
		}
		body {
			background-color:rgb(255, 255, 245);
		}
		</style>	
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b>Petstore Database (Entity Relationship Diagram):</b><br />
					<img src="img/erd.png" width="800" height="800" class="img-responsive center-block" alt="A3 ERD" />

					<br /> <br />
					<b>MySQL Workbench and SQL Files:</b><br />
					<a href="/a3.mwb">Petstore MySQL Workbench File</a>
				<br />
					<a href="/a3.sql">Petstore SQL File</a>				

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		
 
</body>
</html>
