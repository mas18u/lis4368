# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### Assignment #2 Requirements:

*Three Parts*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records per table)
3. Provide Bitbucket read-only access to repo (Language SQL), must include README.MD using markdown syntax, and include links to all the following files.
	- docs folder a3.mwb and a3.sql
	- img folder a3.png (export a3.mwb file as a3.png)
	- README.md (Must display a3.png ERD)
	
*A3 docs: a3.mwb and a3.sql*:  
[A3 MWB File](a3.mwb)  
[A3 SQL File](a3.sql)	


#### Assignment Screenshots:

*Screenshot of A3 ERD*:
![erd] (img/erd.png)

*Screenshot of Index.jsp*:
![index] (img/index.png)