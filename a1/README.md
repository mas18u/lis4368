# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### Assignment #1 Requirements:

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above)
* Screenshot of running http://localhost:9999 (#2 above, step #4b in tutorial)
* git commands with short descriptions
* Bitbucket repo links a)this assignment and b) the completed tutorial above (bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - creates an empty git repository
2. git status - shows the working tree status
3. git add - adds file contents to the index
4. git commit - record changes to the repository
5. git push - update remote refs along with associated objects
6. git pull - fetch from and integrate with another repository or a local branch
7. git clone - downloads existing source code from a remote repository

#### Assignment Screenshots:


*Screenshot of running java Hello*:
![java Hello screenshot] (img/Hello.png)

*Screenshot of running http://localhost9999*:
![localhost] (img/9999.png)

Link to Local LIS4368 web app [http://localhost:9999/lis4368/](http://localhost:9999/lis4368/)

*Screenshot of running local lis4368 web app:
![local site] (img/a1_website.png)


##Chapter Questions (Chs 1-4)
1. D
2. B
3. D
4. A
5. C
6. D
7. A
8. D
9. A
10. D
11. D
12. B
13. C
14. B
15. A
16. D
17. A
18. D
19. A
20. A
21. C
22. B
23. B
24. A







#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/mas18u/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
