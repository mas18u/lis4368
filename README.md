# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### LIS 4368 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
	- Provide screenshot of installations
	- Create Bitbucket repo
	- Complete bitbucket tutorials
	  (bitbucketstationlocations and myteamquotes)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Make and display "Hello" webpage
	- Make and display an index homepage
	- Make a HelloServlet
	- Make a QueryBook servlet that pulls data from external database.  
This website will allow the user to select an author and the page will display the books by that author and how much they cost.

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Entity Relationship Diagram (ERD)
	- Include data (at least 10 records per table)
	- Provide Bitbucket read-only access to repo (Language SQL), must include README.MD using markdown syntax, and include links to all the following files.
		- docs folder a3.mwb and a3.sql
		- img folder a3.png (export a3.mwb file as a3.png)
		- README.md (Must display a3.png ERD)
		
  
4. [P1 README.md](p1/README.md "My P1 README.me file")
	- Main page
	- Implement text boxes on website and input data that updates MySQL database. 
	- Perform basic client-side data validation.  
	- Grade App java program
	

5. [A4 README.md](a4/README.md "My A4 README.md file")  
	- Implement server side data validation for user entered data  
	- Include error page if submitted form is empty  
	- Java Skillset 10 Count Characters  
	- Java Skillset 11 Write/Read count words  
	- Java Skillset 12 ASCII App  

6. [A5 README.md](a5/README.md "My A5 README.md file")  
	- Implement cross side scripting on form  
	- Add user entry from website onto SQL database  
	- Java Skillset 13 Number Swap  
	- Java Skillset 14 Largest of Three Numbers  
	- Java Skillset 15 Simple Calculator Using Methods  

7. [P2 README.md](p2/README.md "My P2 README.md file")  
	- Includes all the same functions as A4, A5, and P1  
	- Add ability for user to modify SQL customer data entry from webform  
	- Add ability to delete customer entry  
