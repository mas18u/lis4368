# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### Project 2 Requirements:

1. Includes all the same functions as A4, A5, and P1
2. Add ability for user to modify SQL customer data entry from webform
3. Add ability to delete customer entry
	
#### Assignment Screenshots:

*Valid User Form Entry customerform.jsp*:  
![Form Entry] (img/validEntry.png)

*Passed Validation thanks.jsp*:  
![passed] (img/passed.png)

*Display Data (customers.jsp)*:  
![Display] (img/displayData.png)  

*Modify Form (modify.jsp)*:  
![Modify] (img/modifyForm.png)  

*Modified Data (customers.jsp)*:  
![Modified] (img/modifiedData.png)  

*Delete Warning (customers.jsp)*:  
![Delete] (img/deleteRecord.png)  

*Associated Database Changes*:  
![Changes] (img/databaseChanges.png)  