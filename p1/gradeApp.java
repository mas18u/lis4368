import java.text.DecimalFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

public class gradeApp
{
	public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        double sum = 0.00;
        double count = 0.00;
        double average = 0.00;
        double currentInput = 0.00;
        DecimalFormat f = new DecimalFormat("##.00");
        

        System.out.println("Please enter grades that range from 0 to 100");
        System.out.println("Grade average and total is rounded to 2 decimal places.");
        System.out.println("Note: Program does *not* check for non-numeric characters.");
        System.out.println("To end program enter -1.\n");

        do
		{
            System.out.print("Enter grade: ");
            currentInput = scnr.nextDouble();
            count++;
			
                if((currentInput > 100) || (currentInput < 0)) {
                    System.out.println("Invalid entry, not counted.");
                    count--;
                    
                }
                else if(currentInput == -1){
                    count--;
                }
                else{
                    sum = sum + currentInput;
                }
                
        } while (currentInput != -1);
        
        System.out.println("\nCount: " + count);
        System.out.println("Total: " + f.format(sum));
        System.out.println("Average: " + f.format((sum/count)));
    
    
}
}