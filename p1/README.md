# LIS 4368 Advanced Web App Development

## Manolo Sanchez

### Project 1 Requirements:


1. Main page
2. Implement text boxes on website and input data that updates MySQL database. 
3. Perform basic client-side data validation.  
4. Grade App java program


#### Assignment Screenshots:

*Screenshot of Home*:
![home] (img/home.png)

*Screenshot of Failed Validation*:
![red] (img/red.png)

*Screenshot of Passed Validaiton*:
![green] (img/green.png)

*Screenshot of Grade App*:
![grade] (img/gradeApp.png)